# Megoldás

## Trend eliminálás

Órán megbeszéltük, hogy azért számolunk a jelenértéki négyzetméterárral, hogy ne zavarjon be az, hogy idő közben emelkedtek a latásárak.

## Tanítás

A következő ábrán látható ahogy egyre több oszlopon tanultak a modellek. Egyértelműen látszik, hogy a `GradientBoostingRegressor` bizonyult a legjobbnak.

![](results.png)

## Változók hatékonysága

A `GradientBoostingRegressor`-nál két jelentősebb ugrás figyelhető meg.
Az eslő a fűtési típus hozzáadásánál van. Elsőre nem számítottam rá, hogy ennyire fontos lehet, de hosszabban átgondolva azért lehet így, mert a fűtési típusok között van olyan, ami nagyon sokat számít az árban. (pl. gáz vs. elektromos). A leghatékonyabb transzformáció a one-hot-encode volt.
A második a nagy ugrás, ami egyben a legnagyobb ugrás volt, az a kerület volt. A kerületek történelmi okokból úgy vannak sorszámozva, hogy a belvárosi kerületek kisebb számokkal rendelkeznek, míg a külső kerületek nagyobb számokkal. A régi kerületek a város belsejében helyezkednek el, itt jellemzően sokkal drágábbak a lakások, ezért a kisebb kerületszám jól képes előrejelzni egy lakás értékét.

További levont következtetés, hogy a `GradientBoostingRegressor` jól tud teljesíteni viszonylag kevés oszlopnyi adattal is, míg a többi model esetében a teljesítmény javulásához sokkel több oszlop szükséges.

Számomra különösen meglepő volt, hogy az irányítószámok hozzáadása nemcsak, hogy rontott, hanem jelentősen rontott a `MLPRegressor` esetében.

## Külső segítség

A feladat megoldása során **GitHub Copilot**-ot használtam, más mesterséges intelligenciát nem.

A **Stack Overflow** és egyéb weboldalakra a hivatkozás a kódban megtalálható.

A munkám végén segítséget kértem **Bálint Gergő**től egy végső átnézésben és korrekciókban ahogy ezt a kitöltött formon is jeleztem.

## Kód

Mivel a feladat kérte, hogy PDF-ben is adjuk be a kódot ezért bemásoltam az összes kódot ide.

### main.py

```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# https://teclado.com/30-days-of-python/python-30-day-21-multiple-files/
from data_preparation import get_preparation_function_lists
from target_variable_creation import target_variable_creation
from train import train, result_export


def main() -> None:
    data: pd.DataFrame = pd.read_csv("data.csv")

    # https://stackoverflow.com/questions/67883198/shuffle-columns-in-dataframe
    data = data[np.random.default_rng(seed=4172787206).permutation(data.columns.values)]

    final_results: dict[str, list[float]] = {}
    prepared = pd.DataFrame()
    for preparation_function_list in get_preparation_function_lists():
        results: dict[str, list[float]] = {}
        for preparation_function in preparation_function_list:
            copy = prepared.copy()
            preparation_function(data, copy)
            target_variable_creation(data, copy)
            train(copy, results)
        best_index = results["GradientBoostingRegressor"].index(
            min(results["GradientBoostingRegressor"])
        )
        preparation_function_list[best_index](data, prepared)
        for name, result in results.items():
            if name not in final_results:
                final_results[name] = []
            final_results[name].append(result[best_index])

    for name, result in final_results.items():
        plt.plot(result, label=name)
    plt.xlabel("Number of features")
    plt.ylabel("MAPE")
    plt.legend()
    plt.savefig("results.png")
    result_export(data, prepared)

if __name__ == "__main__":
    main()
```

### data_preparation.py

```python
# TODO: add this comment for table drop
# https://stackoverflow.com/questions/13411544/delete-a-column-from-a-pandas-dataframe
import pandas as pd


# https://duckduckgo.com/?q=python+roman+converter&atb=v377-1&ia=web
from roman import fromRoman


def city_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["district"] = data["city"].apply(
        lambda x: fromRoman(x[9:-1]) if isinstance(x, str) else 0
    )


def city_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    district = data["city"].apply(
        lambda x: fromRoman(x[9:-1]) if isinstance(x, str) else 0
    )
    prepared["is_at_buda"] = district.apply(lambda x: x in [1, 2, 3, 11, 12, 22])


def city_3(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    district = data["city"].apply(
        lambda x: fromRoman(x[9:-1]) if isinstance(x, str) else 0
    )
    prepared["is_at_pest"] = district.apply(
        lambda x: x in [4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23]
    )


def postcode_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["postcode"] = data["postcode"].fillna(1000)


def postcode_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["district_from_postcode"] = (
        data["postcode"].fillna(1000).apply(lambda x: x % 1000 // 10)
    )


def property_subtype(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["is_brick"] = data["property_subtype"].apply(
        lambda x: isinstance(x, str) and "brick" in x
    )


def property_condition_type_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    conditions = [
        "good",
        "novel",
        "renewed",
        "medium",
        "new_construction",
        "to_be_renovated",
        "can_move_in",
        "under_construction",
    ]
    for condition in conditions:
        prepared[f"condition_is_{condition}"] = data["property_condition_type"].apply(
            lambda x, condition: isinstance(x, str) and x == condition,
            args=(condition,),
        )


def property_condition_type_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    condition_mappings = {
        "good": 3,
        "novel": 2,
        "renewed": 1,
        "medium": 0,
        "new_construction": 4,
        "to_be_renovated": -3,
        "can_move_in": -2,
        "under_construction": -1,
        "missing_info": 0,
    }
    prepared["condition_number"] = data["property_condition_type"].map(
        condition_mappings
    )


def property_floor_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dictionary = {
        "basement": -1,
        "ground floor": 0,
        "mezzanine floor": 0.5,
        "10 plus": 11,
    }

    def f(x):
        try:
            return int(x)
        except ValueError:
            try:
                return dictionary[x]
            except KeyError:
                return 0

    prepared["property_floor"] = data["property_floor"].apply(f)


def property_floor_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dictionary = {
        "basement": -1,
        "ground floor": 0,
        "mezzanine floor": 0.5,
        "10 plus": 11,
    }

    def f(x):
        try:
            return int(x)
        except ValueError:
            try:
                return dictionary[x]
            except KeyError:
                return 0

    property_floor = data["property_floor"].apply(f)
    prepared["5_plus_floor"] = property_floor.apply(lambda x: x >= 5)


def building_floor_count(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    def f(x):
        try:
            return int(x)
        except ValueError:
            return 11 if x == "more than 10" else 1

    prepared["building_floor_count"] = data["building_floor_count"].apply(f)


def view_type(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    view_types = ["street view", "garden view", "panoramic", "courtyard view"]
    for view_type in view_types:
        prepared[f"view_type_is_{view_type}"] = data["view_type"].apply(
            lambda x, view_type: isinstance(x, str) and x == view_type,
            args=(view_type,),
        )


import math


def orientation_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    orientation_to_degree = {
        "north": 0,
        "north-east": 45,
        "east": 90,
        "south-east": 135,
        "south": 180,
        "south-west": 225,
        "west": 270,
        "north-west": 315,
    }

    # https://www.w3schools.com/python/ref_math_sin.asp
    prepared["orientation_sin"] = data["orientation"].apply(
        lambda x: math.sin(math.radians(orientation_to_degree[x]))
        if isinstance(x, str)
        else 0
    )
    prepared["orientation_cos"] = data["orientation"].apply(
        lambda x: math.cos(math.radians(orientation_to_degree[x]))
        if isinstance(x, str)
        else 0
    )


def orientation_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    orientations = [
        "north",
        "north-east",
        "east",
        "south-east",
        "south",
        "south-west",
        "west",
        "north-west",
    ]
    for orientation in orientations:
        prepared[f"orientation_is_{orientation}"] = data["orientation"].apply(
            lambda x, orientation: isinstance(x, str) and x == orientation,
            args=(orientation,),
        )


def garden_access(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["has_garden_access"] = data["garden_access"].apply(
        lambda x: isinstance(x, str) and x == "yes"
    )


def heating_type_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    heating_types = [
        "tile stove (gas)",
        "konvection gas burner",
        "central heating with own meter",
        "central heating",
        "district heating",
        "gas furnace, circulating hot water",
        "electric",
        "other",
        "fan-coil",
        "gas furnace",
    ]
    for heating_type in heating_types:
        prepared[f"heating_type_is_{heating_type}"] = data["heating_type"].apply(
            lambda x, heating_type: isinstance(x, str) and x == heating_type,
            args=(heating_type,),
        )


def heating_type_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    heating_type_map = {
        "tile stove (gas)": 0,
        "konvection gas burner": 1,
        "central heating with own meter": 2,
        "central heating": 3,
        "district heating": 4,
        "gas furnace, circulating hot water": 5,
        "electric": 6,
        "other": 7,
        "fan-coil": 8,
        "gas furnace": 9,
    }
    prepared["heating_type"] = (
        data["heating_type"].fillna("other").map(heating_type_map)
    )


def elevator_type(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["has_elevator"] = data["elevator_type"].apply(
        lambda x: isinstance(x, str) and x == "yes"
    )


def created_at_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dates = pd.to_datetime(data["created_at"])
    min_date = dates.min()
    days_since_first = (dates - min_date).dt.days
    prepared["day_of_week"] = 1 + days_since_first & 7


def created_at_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dates = pd.to_datetime(data["created_at"])
    min_date = dates.min()
    days_since_first = (dates - min_date).dt.days
    day_of_week = 1 + days_since_first & 7
    prepared["is_weekend"] = day_of_week >= 6


from typing import Callable


def get_preparation_function_lists() -> (
    list[list[Callable[[pd.DataFrame, pd.DataFrame], None]]]
):
    # The order is based on my random number
    return [
        [orientation_1, orientation_2],
        [view_type],
        [heating_type_1, heating_type_2],
        [property_floor_1, property_floor_2],
        [created_at_1, created_at_2],
        [city_1, city_2, city_3],
        [building_floor_count],
        [property_condition_type_1, property_condition_type_2],
        [elevator_type],
        [property_subtype],
        [garden_access],
        [postcode_1, postcode_2],
    ]
```

### target_variable_creation.py

```python
import pandas as pd
from sklearn.linear_model import LinearRegression


def target_variable_creation(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dates = pd.to_datetime(data["created_at"])
    min_date = dates.min()
    data["days_since_first"] = (dates - min_date).dt.days
    data["price_per_square"] = (
        data["price_created_at"] * 1_000_000 / data["property_area"]
    )
    aggregated = data.groupby("days_since_first", as_index=False).agg(
        {"price_per_square": "median"}
    )
    x_train = aggregated[["days_since_first"]]
    y_train = aggregated["price_per_square"]
    linear_regression = LinearRegression()
    linear_regression.fit(x_train, y_train)
    y_pred = linear_regression.predict(x_train)
    aggregated["price_per_square_prediction"] = y_pred
    aggregated.drop("price_per_square", axis=1, inplace=True)
    data = data.merge(aggregated, on="days_since_first", how="left")
    prepared["corrected_price_per_square"] = (
        data["price_per_square"]
        * data["price_per_square_prediction"].max()
        / data["price_per_square_prediction"]
    )
    data.drop(["price_per_square", "price_per_square_prediction"], axis=1, inplace=True)
```

### train.py

```python
import pandas as pd

# https://scikit-learn.org/stable/modules/model_evaluation.html
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error


def evaluate(
    y_true: pd.Series, y_pred: pd.Series, name: str, results: dict[str, list[float]]
) -> None:
    print(f"Evaluating: {name}")
    MAE = mean_absolute_error(y_true, y_pred)
    print(f"    MAE: {MAE}")
    RMSE = mean_squared_error(y_true, y_pred, squared=False)
    print(f"    RMSE: {RMSE}")
    MAPE = mean_absolute_percentage_error(y_true, y_pred)
    print(f"    MAPE: {MAPE}")
    if name not in results:
        results[name] = []
    results[name].append(MAPE)
    print()


# https://online.stat.psu.edu/stat508/book/export/html/752
from sklearn.model_selection import KFold


def train_with(data: pd.DataFrame, model: any, results: dict[str, list[float]]) -> None:
    kf = KFold(n_splits=3, shuffle=True, random_state=4172787206)
    y_preds = pd.Series()
    for train_index, test_index in kf.split(data):
        x_train, x_test = data.iloc[train_index, :-1], data.iloc[test_index, :-1]
        y_train = data.iloc[train_index, -1]
        model.fit(x_train, y_train)
        y_pred = pd.Series(model.predict(x_test), index=test_index)
        y_preds = pd.concat([y_preds, y_pred])
    evaluate(data.iloc[y_preds.index, -1], y_preds, model.__class__.__name__, results)


from sklearn.linear_model import LinearRegression
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.neural_network import MLPRegressor


def train(data: pd.DataFrame, results: dict[str, list[float]]) -> None:
    models: list[any] = [
        LinearRegression(),
        GradientBoostingRegressor(random_state=4172787206),
        MLPRegressor(
            random_state=4172787206,
            hidden_layer_sizes=(25, 13),
            max_iter=100,
            learning_rate="adaptive",
            early_stopping=True,
        ),
    ]
    for model in models:
        train_with(data, model, results)


def result_export(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    model = GradientBoostingRegressor(
        random_state=4172787206, max_depth=6, n_estimators=200
    )

    kf = KFold(n_splits=3, shuffle=True, random_state=4172787206)
    y_preds = pd.Series()
    for train_index, test_index in kf.split(prepared):
        x_train, x_test = (
            prepared.iloc[train_index, :-1],
            prepared.iloc[test_index, :-1],
        )
        y_train = prepared.iloc[train_index, -1]
        model.fit(x_train, y_train)
        y_pred = pd.Series(model.predict(x_test), index=test_index)
        y_preds = pd.concat([y_preds, y_pred])

    y_true = prepared.iloc[y_preds.index, -1]

    df = pd.DataFrame({"y_true": y_true, "y_pred": y_pred})
    pct_diff = (df["y_pred"] - df["y_true"]) / df["y_true"] * 100
    mask = (pct_diff >= 5) & (pct_diff <= 25)
    filtered_df = df[mask]
    data.filter(items=filtered_df.index, axis=0).to_csv(
        "Results_MM5NOT.csv", index=False
    )
```
