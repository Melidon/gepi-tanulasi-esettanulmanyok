import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# https://teclado.com/30-days-of-python/python-30-day-21-multiple-files/
from data_preparation import get_preparation_function_lists
from target_variable_creation import target_variable_creation
from train import train, result_export


def main() -> None:
    data: pd.DataFrame = pd.read_csv("data.csv")

    # https://stackoverflow.com/questions/67883198/shuffle-columns-in-dataframe
    data = data[np.random.default_rng(seed=4172787206).permutation(data.columns.values)]

    final_results: dict[str, list[float]] = {}
    prepared = pd.DataFrame()
    for preparation_function_list in get_preparation_function_lists():
        results: dict[str, list[float]] = {}
        for preparation_function in preparation_function_list:
            copy = prepared.copy()
            preparation_function(data, copy)
            target_variable_creation(data, copy)
            train(copy, results)
        best_index = results["GradientBoostingRegressor"].index(
            min(results["GradientBoostingRegressor"])
        )
        preparation_function_list[best_index](data, prepared)
        for name, result in results.items():
            if name not in final_results:
                final_results[name] = []
            final_results[name].append(result[best_index])

    for name, result in final_results.items():
        plt.plot(result, label=name)
    plt.xlabel("Number of features")
    plt.ylabel("MAPE")
    plt.legend()
    plt.savefig("results.png")
    result_export(data, prepared)

if __name__ == "__main__":
    main()
