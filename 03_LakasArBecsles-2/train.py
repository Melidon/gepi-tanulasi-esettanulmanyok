import pandas as pd

# https://scikit-learn.org/stable/modules/model_evaluation.html
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error


def evaluate(
    y_true: pd.Series, y_pred: pd.Series, name: str, results: dict[str, list[float]]
) -> None:
    print(f"Evaluating: {name}")
    MAE = mean_absolute_error(y_true, y_pred)
    print(f"    MAE: {MAE}")
    RMSE = mean_squared_error(y_true, y_pred, squared=False)
    print(f"    RMSE: {RMSE}")
    MAPE = mean_absolute_percentage_error(y_true, y_pred)
    print(f"    MAPE: {MAPE}")
    if name not in results:
        results[name] = []
    results[name].append(MAPE)
    print()


# https://online.stat.psu.edu/stat508/book/export/html/752
from sklearn.model_selection import KFold


def train_with(data: pd.DataFrame, model: any, results: dict[str, list[float]]) -> None:
    kf = KFold(n_splits=3, shuffle=True, random_state=4172787206)
    y_preds = pd.Series()
    for train_index, test_index in kf.split(data):
        x_train, x_test = data.iloc[train_index, :-1], data.iloc[test_index, :-1]
        y_train = data.iloc[train_index, -1]
        model.fit(x_train, y_train)
        y_pred = pd.Series(model.predict(x_test), index=test_index)
        y_preds = pd.concat([y_preds, y_pred])
    evaluate(data.iloc[y_preds.index, -1], y_preds, model.__class__.__name__, results)


from sklearn.linear_model import LinearRegression
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.neural_network import MLPRegressor


def train(data: pd.DataFrame, results: dict[str, list[float]]) -> None:
    models: list[any] = [
        LinearRegression(),
        GradientBoostingRegressor(random_state=4172787206),
        MLPRegressor(
            random_state=4172787206,
            hidden_layer_sizes=(25, 13),
            max_iter=100,
            learning_rate="adaptive",
            early_stopping=True,
        ),
    ]
    for model in models:
        train_with(data, model, results)


def result_export(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    model = GradientBoostingRegressor(
        random_state=4172787206, max_depth=6, n_estimators=200
    )

    kf = KFold(n_splits=3, shuffle=True, random_state=4172787206)
    y_preds = pd.Series()
    for train_index, test_index in kf.split(prepared):
        x_train, x_test = (
            prepared.iloc[train_index, :-1],
            prepared.iloc[test_index, :-1],
        )
        y_train = prepared.iloc[train_index, -1]
        model.fit(x_train, y_train)
        y_pred = pd.Series(model.predict(x_test), index=test_index)
        y_preds = pd.concat([y_preds, y_pred])

    y_true = prepared.iloc[y_preds.index, -1]

    df = pd.DataFrame({"y_true": y_true, "y_pred": y_pred})
    pct_diff = (df["y_pred"] - df["y_true"]) / df["y_true"] * 100
    mask = (pct_diff >= 5) & (pct_diff <= 25)
    filtered_df = df[mask]
    data.filter(items=filtered_df.index, axis=0).to_csv(
        "Results_MM5NOT.csv", index=False
    )
