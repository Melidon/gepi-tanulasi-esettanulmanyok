import pandas as pd
from sklearn.linear_model import LinearRegression


def target_variable_creation(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dates = pd.to_datetime(data["created_at"])
    min_date = dates.min()
    data["days_since_first"] = (dates - min_date).dt.days
    data["price_per_square"] = (
        data["price_created_at"] * 1_000_000 / data["property_area"]
    )
    aggregated = data.groupby("days_since_first", as_index=False).agg(
        {"price_per_square": "median"}
    )
    x_train = aggregated[["days_since_first"]]
    y_train = aggregated["price_per_square"]
    linear_regression = LinearRegression()
    linear_regression.fit(x_train, y_train)
    y_pred = linear_regression.predict(x_train)
    aggregated["price_per_square_prediction"] = y_pred
    aggregated.drop("price_per_square", axis=1, inplace=True)
    data = data.merge(aggregated, on="days_since_first", how="left")
    prepared["corrected_price_per_square"] = (
        data["price_per_square"]
        * data["price_per_square_prediction"].max()
        / data["price_per_square_prediction"]
    )
    data.drop(["price_per_square", "price_per_square_prediction"], axis=1, inplace=True)
