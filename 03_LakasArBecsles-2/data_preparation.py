# TODO: add this comment for table drop
# https://stackoverflow.com/questions/13411544/delete-a-column-from-a-pandas-dataframe
import pandas as pd


# https://duckduckgo.com/?q=python+roman+converter&atb=v377-1&ia=web
from roman import fromRoman


def city_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["district"] = data["city"].apply(
        lambda x: fromRoman(x[9:-1]) if isinstance(x, str) else 0
    )


def city_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    district = data["city"].apply(
        lambda x: fromRoman(x[9:-1]) if isinstance(x, str) else 0
    )
    prepared["is_at_buda"] = district.apply(lambda x: x in [1, 2, 3, 11, 12, 22])


def city_3(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    district = data["city"].apply(
        lambda x: fromRoman(x[9:-1]) if isinstance(x, str) else 0
    )
    prepared["is_at_pest"] = district.apply(
        lambda x: x in [4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23]
    )


def postcode_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["postcode"] = data["postcode"].fillna(1000)


def postcode_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["district_from_postcode"] = (
        data["postcode"].fillna(1000).apply(lambda x: x % 1000 // 10)
    )


def property_subtype(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["is_brick"] = data["property_subtype"].apply(
        lambda x: isinstance(x, str) and "brick" in x
    )


def property_condition_type_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    conditions = [
        "good",
        "novel",
        "renewed",
        "medium",
        "new_construction",
        "to_be_renovated",
        "can_move_in",
        "under_construction",
    ]
    for condition in conditions:
        prepared[f"condition_is_{condition}"] = data["property_condition_type"].apply(
            lambda x, condition: isinstance(x, str) and x == condition,
            args=(condition,),
        )


def property_condition_type_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    condition_mappings = {
        "good": 3,
        "novel": 2,
        "renewed": 1,
        "medium": 0,
        "new_construction": 4,
        "to_be_renovated": -3,
        "can_move_in": -2,
        "under_construction": -1,
        "missing_info": 0,
    }
    prepared["condition_number"] = data["property_condition_type"].map(
        condition_mappings
    )


def property_floor_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dictionary = {
        "basement": -1,
        "ground floor": 0,
        "mezzanine floor": 0.5,
        "10 plus": 11,
    }

    def f(x):
        try:
            return int(x)
        except ValueError:
            try:
                return dictionary[x]
            except KeyError:
                return 0

    prepared["property_floor"] = data["property_floor"].apply(f)


def property_floor_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dictionary = {
        "basement": -1,
        "ground floor": 0,
        "mezzanine floor": 0.5,
        "10 plus": 11,
    }

    def f(x):
        try:
            return int(x)
        except ValueError:
            try:
                return dictionary[x]
            except KeyError:
                return 0

    property_floor = data["property_floor"].apply(f)
    prepared["5_plus_floor"] = property_floor.apply(lambda x: x >= 5)


def building_floor_count(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    def f(x):
        try:
            return int(x)
        except ValueError:
            return 11 if x == "more than 10" else 1

    prepared["building_floor_count"] = data["building_floor_count"].apply(f)


def view_type(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    view_types = ["street view", "garden view", "panoramic", "courtyard view"]
    for view_type in view_types:
        prepared[f"view_type_is_{view_type}"] = data["view_type"].apply(
            lambda x, view_type: isinstance(x, str) and x == view_type,
            args=(view_type,),
        )


import math


def orientation_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    orientation_to_degree = {
        "north": 0,
        "north-east": 45,
        "east": 90,
        "south-east": 135,
        "south": 180,
        "south-west": 225,
        "west": 270,
        "north-west": 315,
    }

    # https://www.w3schools.com/python/ref_math_sin.asp
    prepared["orientation_sin"] = data["orientation"].apply(
        lambda x: math.sin(math.radians(orientation_to_degree[x]))
        if isinstance(x, str)
        else 0
    )
    prepared["orientation_cos"] = data["orientation"].apply(
        lambda x: math.cos(math.radians(orientation_to_degree[x]))
        if isinstance(x, str)
        else 0
    )


def orientation_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    orientations = [
        "north",
        "north-east",
        "east",
        "south-east",
        "south",
        "south-west",
        "west",
        "north-west",
    ]
    for orientation in orientations:
        prepared[f"orientation_is_{orientation}"] = data["orientation"].apply(
            lambda x, orientation: isinstance(x, str) and x == orientation,
            args=(orientation,),
        )


def garden_access(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["has_garden_access"] = data["garden_access"].apply(
        lambda x: isinstance(x, str) and x == "yes"
    )


def heating_type_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    heating_types = [
        "tile stove (gas)",
        "konvection gas burner",
        "central heating with own meter",
        "central heating",
        "district heating",
        "gas furnace, circulating hot water",
        "electric",
        "other",
        "fan-coil",
        "gas furnace",
    ]
    for heating_type in heating_types:
        prepared[f"heating_type_is_{heating_type}"] = data["heating_type"].apply(
            lambda x, heating_type: isinstance(x, str) and x == heating_type,
            args=(heating_type,),
        )


def heating_type_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    heating_type_map = {
        "tile stove (gas)": 0,
        "konvection gas burner": 1,
        "central heating with own meter": 2,
        "central heating": 3,
        "district heating": 4,
        "gas furnace, circulating hot water": 5,
        "electric": 6,
        "other": 7,
        "fan-coil": 8,
        "gas furnace": 9,
    }
    prepared["heating_type"] = (
        data["heating_type"].fillna("other").map(heating_type_map)
    )


def elevator_type(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    prepared["has_elevator"] = data["elevator_type"].apply(
        lambda x: isinstance(x, str) and x == "yes"
    )


def created_at_1(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dates = pd.to_datetime(data["created_at"])
    min_date = dates.min()
    days_since_first = (dates - min_date).dt.days
    prepared["day_of_week"] = 1 + days_since_first & 7


def created_at_2(data: pd.DataFrame, prepared: pd.DataFrame) -> None:
    dates = pd.to_datetime(data["created_at"])
    min_date = dates.min()
    days_since_first = (dates - min_date).dt.days
    day_of_week = 1 + days_since_first & 7
    prepared["is_weekend"] = day_of_week >= 6


from typing import Callable


def get_preparation_function_lists() -> (
    list[list[Callable[[pd.DataFrame, pd.DataFrame], None]]]
):
    # The order is based on my random number
    return [
        [orientation_1, orientation_2],
        [view_type],
        [heating_type_1, heating_type_2],
        [property_floor_1, property_floor_2],
        [created_at_1, created_at_2],
        [city_1, city_2, city_3],
        [building_floor_count],
        [property_condition_type_1, property_condition_type_2],
        [elevator_type],
        [property_subtype],
        [garden_access],
        [postcode_1, postcode_2],
    ]
