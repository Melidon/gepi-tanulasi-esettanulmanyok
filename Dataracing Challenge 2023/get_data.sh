#!/bin/bash

rm -rf data
mkdir data
cd data
wget --output-document="dataracing2023datacollection.zip" "https://drive.google.com/u/0/uc?id=17-k5lBWsBudijdfAJ6oJxwVXMd_EzVlL&export=download&confirm=t&uuid=5370645c-16a1-45e1-95a6-a19ae0ecf2e3&at=AB6BwCDT3H_3Lztk0i0ZZu29bDnZ:1699271219180"
unzip -P data23 dataracing2023datacollection.zip
rm dataracing2023datacollection.zip
chmod 444 training_data.csv
chmod 444 data_submission_example.csv
cd ..
