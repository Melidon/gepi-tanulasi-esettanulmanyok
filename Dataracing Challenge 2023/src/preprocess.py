import pandas as pd


def shift_dates(df: pd.DataFrame) -> pd.DataFrame:
    COL = "CONTRACT_DATE_OF_LOAN_AGREEMENT"
    min_date = df[COL].min()
    df[COL] = df[COL] - min_date
    df["CONTRACT_MATURITY_DATE"] = df["CONTRACT_MATURITY_DATE"] - min_date
    df["TARGET_EVENT_DAY"] = df["TARGET_EVENT_DAY"] - min_date
    return df


def to_dummies(df: pd.DataFrame, col: str) -> pd.DataFrame:
    return pd.get_dummies(df[col], prefix=col)


def contract_bank_id(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_BANK_ID")


def contract_credit_intermediary(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_CREDIT_INTERMEDIARY")


def contract_credit_loss(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_CREDIT_LOSS"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_currency(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_CURRENCY")


def contract_date_of_loan_agreement(df: pd.DataFrame) -> pd.Series:
    return df["CONTRACT_DATE_OF_LOAN_AGREEMENT"]


def contract_dept_service_to_income(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_DEPT_SERVICE_TO_INCOME"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_frequency_type(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_FREQUENCY_TYPE")


def contract_income(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_INCOME"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_instalment_amount(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_INSTALMENT_AMOUNT"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_instalment_amount_2(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_INSTALMENT_AMOUNT_2"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_interest_period(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_INTEREST_PERIOD"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_interest_rate(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_INTEREST_RATE"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_lgd(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_LGD"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_loan_amount(df: pd.DataFrame) -> pd.Series:
    return df["CONTRACT_LOAN_AMOUNT"]


def contract_loan_contract_type(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_LOAN_CONTRACT_TYPE")


def contract_loan_to_value_ratio(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_LOAN_TO_VALUE_RATIO"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_loan_type(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_LOAN_TYPE")


def contract_market_value(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_MARKET_VALUE"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_maturity_date(df: pd.DataFrame) -> pd.Series:
    return df["CONTRACT_MATURITY_DATE"]


def contract_mortgage_lending_value(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_MORTGAGE_LENDING_VALUE"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_mortgage_type(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_MORTGAGE_TYPE")


def contract_refinanced(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_REFINANCED")


def contract_risk_weighted_assets(df: pd.DataFrame) -> pd.Series:
    COL = "CONTRACT_RISK_WEIGHTED_ASSETS"
    median = df[COL].median()
    return df[COL].fillna(median)


def contract_type_of_interest_repayment(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "CONTRACT_TYPE_OF_INTEREST_REPAYMENT")


def borrower_birth_year(df: pd.DataFrame) -> pd.Series:
    COL = "BORROWER_BIRTH_YEAR"
    max_year = df[COL].max()
    median = df[COL].median()
    return pd.Series(max_year + 18 - df[COL], name="BORROWER_AGE").fillna(median)


def borrower_citizenship(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "BORROWER_CITIZENSHIP")


def borrower_country(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "BORROWER_COUNTRY")


def borrower_county(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "BORROWER_COUNTY")


def borrower_type_of_customer(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "BORROWER_TYPE_OF_CUSTOMER")


def borrower_type_of_settlement(df: pd.DataFrame) -> pd.DataFrame:
    return to_dummies(df, "BORROWER_TYPE_OF_SETTLEMENT")


def borrower_contract_count(df: pd.DataFrame) -> pd.DataFrame:
    borrower_contract_counts = df.groupby("BORROWER_ID").agg({"CONTRACT_ID": "count"})
    borrower_contract_counts.columns = ["BORROWER_CONTRACT_COUNT"]
    result = pd.merge(
        df["BORROWER_ID"],
        borrower_contract_counts,
        on="BORROWER_ID",
        how="left",
        validate="many_to_one",
    )
    return result["BORROWER_CONTRACT_COUNT"]


def borrower_loan_amount_sum(df: pd.DataFrame) -> pd.DataFrame:
    borrower_loan_amount_sums = df.groupby("BORROWER_ID").agg(
        {"CONTRACT_LOAN_AMOUNT": "sum"}
    )
    borrower_loan_amount_sums.columns = ["BORROWER_LOAN_AMOUNT_SUM"]
    result = pd.merge(
        df["BORROWER_ID"],
        borrower_loan_amount_sums,
        on="BORROWER_ID",
        how="left",
        validate="many_to_one",
    )
    return result["BORROWER_LOAN_AMOUNT_SUM"]


def get_features(df: pd.DataFrame) -> pd.DataFrame:
    functions = [
        contract_bank_id,
        contract_credit_intermediary,
        contract_credit_loss,
        contract_currency,
        # contract_date_of_loan_agreement,
        contract_dept_service_to_income,
        contract_frequency_type,
        contract_income,
        contract_instalment_amount,
        contract_instalment_amount_2,
        contract_interest_period,
        contract_interest_rate,
        contract_lgd,
        contract_loan_amount,
        contract_loan_contract_type,
        contract_loan_to_value_ratio,
        contract_loan_type,
        contract_market_value,
        # contract_maturity_date,
        contract_mortgage_lending_value,
        contract_mortgage_type,
        contract_refinanced,
        contract_risk_weighted_assets,
        contract_type_of_interest_repayment,
        borrower_birth_year,
        borrower_citizenship,
        borrower_country,
        borrower_county,
        borrower_type_of_customer,
        borrower_type_of_settlement,
        borrower_contract_count,
        borrower_loan_amount_sum,
    ]
    return pd.concat([f(df) for f in functions], axis=1)


def get_target(df: pd.DataFrame) -> pd.DataFrame:
    defaulted = pd.Series(df["TARGET_EVENT"] == "K", name="DEFAULTED")
    return pd.DataFrame(defaulted)


def first_year(df: pd.DataFrame) -> pd.DataFrame:
    return df[df["CONTRACT_DATE_OF_LOAN_AGREEMENT"] < 365]
